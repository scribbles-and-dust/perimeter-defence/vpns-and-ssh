#!/etc/bash
# knock receiver script - activated by knockd

# 1. grab the knocking IP
knocker=$1
sshPort=SSHPORT

# 2. add to ignore list in fail2ban. Even if this is temporary until service restart / reload the knocker will allow reenablement
fail2ban-client set sshd addignoreip $knocker
fail2ban-client set portscan addignoreip $knocker

# 3. add to limit list in ufw (if not already added)
checkExisting=$(iptables -L -n | grep -i "$knocker" | grep -i "accept")
if [ -z "$checkExisting" ]; then
	# not already added
	ufw limit in from $knocker proto tcp to any port $sshPort comment "Added by automated knocker"
fi
