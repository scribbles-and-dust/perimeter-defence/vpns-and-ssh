# Summary
During the course of maintaining high risk VPS, it's often comforting to know that I've chosed VPN providers so true to privacy ideals that hackers also use them. The inevitable symptom is that a multi-layered defence would identify the IPs originating the port scan or attack attempt, and block them.

This can unfortunately also lock you out of your own VPS if you're using the same IP range / VPN provider.

Instead of having to use the web-based KVM from your cloud provider to enable your latest IP again, this knock method simple allows a limited connectivity to the custom SSH port of your choosing.

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# Solution
A combination of scripting and configuration is needed across the following tools:
1. fail2ban
2. knockd (port knocking service)
3. UFW used in this instance for speed of setup. I'll migrate to a more generic iptables version in future

## fail2ban Configuration
In this example I've applied the rules to the SSH and portscan jails. The knockd trigger script adds the knocker IP to the sshd and portscan ignoreip configs, although this appears to be temporary (until the fail2ban service is restarted).

I've added these specific jail configs from trigger.sh in case you don't already have them defined.

## knockd Server Configuration
You'll need to think up a sequence of ports, which won't get detected and blocked by the defence mechanisms on the target server too easily. Example included in knockd.conf which uses two ports in combinations. The command here is our trigger script supplied with the knocker IP as it's first arg

### knockd Trigger Script
trigger.sh can be found in the root of this project

## knockd Client
simply install knockd on your client machine, and use:
> knock PORT:tcp PORT:tcp ... -v -d 10

This keeps a 10 millisend delay between sends to ensure consistent sequence at arrival point.